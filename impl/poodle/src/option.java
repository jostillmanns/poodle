
public class option {
	String value;
	int optionsid;
	
	public String getValue(){return value;}
	public int getOptionsID(){return optionsid;}
	
	public option(String value, int pollid){
		this.optionsid = pollid;
		this.value = value;
	}
	public option(String value){
		this.value = value;
	}
}
