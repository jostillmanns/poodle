
public class poll {
	String title;
	String description;
	String due_date;
	String passwd;
	
	public String getTitle() {return title;}
	public String getDescription() {return description;}
	public String getDue_Date() {return due_date;}
	public String getPassWD() {return passwd;}
	
	public poll(String title, String description, String due_date, String passwd){
		this.title = title;
		this.description = description;
		this.due_date = due_date;
		this.passwd = passwd;
	}
}
