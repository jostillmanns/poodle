import java.util.ArrayList;
import java.util.List;
import java.io.Console;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Connection;
import java.sql.Statement;


public class DBFacade {
	private Connection dbConnection;
	
	public DBFacade() {
		String url = "localhost";
		String username= "root";
		String password = "root";
		String schema = "poodle";
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			dbConnection = DriverManager.getConnection("jdbc:mysql://" + url + "/" + schema , username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createPoll(poll p, ArrayList<option> options){
		
		try {
			Statement stmt = dbConnection.createStatement();
			String query = "INSERT INTO poll (title, description) VALUES (\"" + p.getTitle() + "\", \""+ p.getDescription() +"\")";		
			stmt.executeUpdate(query);
			
			ResultSet result = dbConnection.createStatement().executeQuery("SELECT LAST_INSERT_ID() FROM poll");
			int pollid =0;
			while(result.next()) 
				pollid=result.getInt("LAST_INSERT_ID()");
			for(int i = 0; i < options.size(); i++){
				stmt.executeUpdate("INSERT INTO polloption (idpoll, optionvalue) VALUES (" + pollid + ", \"" + options.get(i).getValue() + "\")");
			}
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<option> getOptions(int pollid)
		{
		ArrayList<option> options = new ArrayList<option>();
		try {
			
			ResultSet result = dbConnection.createStatement().executeQuery("SELECT * FROM polloption where idpoll = "+pollid);
			
			while(result.next()){
				options.add(new option(result.getString(3), result.getInt(1)));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return options;
	}
	
	public ArrayList getVotes(int pollid, int optionid)
	{
	ArrayList<option> votes = new ArrayList<option>();
	try {
		
		ResultSet result = dbConnection.createStatement().executeQuery("SELECT * FROM option where idpoll = "+pollid + "AND idoption = "+optionid);
		
		while(result.next()){
			votes.add(new option(result.getString(3), result.getInt(1)));
		}
	
	} catch (SQLException e) {
		e.printStackTrace();
	}	
	return votes;
}
	
	public poll getPoll(int index){
		try {
			ResultSet result = dbConnection.createStatement().executeQuery("SELECT * FROM poll where idpoll = "+index);
			while (result.next())
			{
				if(result.getInt(1) == index){
					return new poll(result.getString(2), result.getString(3), result.getString(4), result.getString(5));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void vote(int pollid, int optionid, vote v){
		try {
			dbConnection.createStatement().executeUpdate("INSERT INTO vote (idpoll, idoption, nickname) VALUE (" + pollid + ", " + optionid + ", " + v.getNickname() +")");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
