* method specifications
** public methods
*** createPoll (poll p, ArrayList<option> options) returns void
    - preconditions: 
      - p != null
      - options.size() >= 1
      - options != null
    - postconditions:
      - poodle.poll@pre -> size() +1 = poodle.poll -> size()
      - poodle.otions@pre -> size() + options -> size() = poodle.options.size()
    - adds a new poll to poll schema
    - adds new options to options schema
