* Normal Speech
  - poodle should be developed
  - poodle is implemented in a webserver (done)
  - polls exist (done)
    - poll can be voted on (done)
    - poll can be published (done)
    - poll consist of (done)
      - title
      - description
      - link
      - due date
      - optional password restriction
  - poll has minimum of one option (done)
  - votes exist (done)
  - user can create polls -> user becomes polleditor (done)
  - vote consist of (done)
    - option
    - nickname
  - due date is set to 3 month no use of poll (done)
  - statistics consist of (done)
    - polls
    - votes per option
    - votes total
  - histogram consist of (done)
    - current statistic (daily basis)
    - optional password restriction
  - pollcreator can edit polls (done)
  - poodle can send emails with poll links (done)
  - poodle can send messages to users (done)
  - registered users exist (done)
  - user can register -> user becomes registered user (done)
  - user can login -> becomes registered user (done)
  - registered user can browse polls they created (done)
  - poll creator select the right options (done)
  - vote of a user is always correct (done)
  - there are no double votes (done)
* Assumptions
  (A1) vote of a user is always correct
  (A2) there are no double votes
  (A3) poll creator select the right options  - poll creator select the right options (done)
  (A4) a poll consist of minimum one option
  (A5) an link of a poll is unique
  (A6) poodle is implemented as a Web 2.0 aaplication
  (A7) when user logins / registers user becomes registered_user
  (A8) users who create poll become pollcreator
  (A9) when poll is not used for 3 month due date is set to current date
* Facts
* Requirments
  (R1) user can create polls 
  (R2) pollcreator can edit polls
  (R3) users can vote
  (R4) user can register
  (R5) user can login
  (R6) registered_users can browse polls they created
  (R7) when due date arrives poll is destroyed
  (R8) user can get poll with link
* Domains
** Biddable Domains
   - user
   - poll creator
   - registered user
** lexical domains
   - poll
   - vote
   - due date
   - statistic
   - histogram
   - userdb
   - webpage
     
