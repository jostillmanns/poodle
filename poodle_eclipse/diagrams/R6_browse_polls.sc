DiagramStyle: uml
DiagramName: R6_browse_polls
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:42:18 CET 2012
Version: ""
PrintFootLine: no
actor: u, registered_user, "", 50, 50
process: m, browse_polls_machine, "", 50, 50
process: p, poll, "", 50, 50
process: w, webpage, "", 50, 50
msg: u, m, browse_polls;
regionbegin: m, activation;
msg: m, p, get_polls
regionbegin: p, activation;
regionend: p
msg:p, m, poll_list,*;
msg: m, w, show_poll_list
regionbegin: w, activation;
regionend:w
msg: w, m, ack, *;
regionend:m;
