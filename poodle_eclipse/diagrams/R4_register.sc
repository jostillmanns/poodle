DiagramStyle: uml
DiagramName: R4_register
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:41:56 CET 2012
Version: ""
PrintFootLine: no
actor: u, user, "", 50, 50
process: m, register_machine, "", 50, 50
process: p, user_database, "", 50, 50
state: m, !user_exists;
msg: u, m, register;
msg: m, p, making_user
regionbegin: p, activation;
;
;
regionend: p
msg: p,m, "", *;
msg: m, u, confirm_create_user;
state: m, user_exists
