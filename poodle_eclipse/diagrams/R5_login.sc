DiagramStyle: uml
DiagramName: R5_login
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:42:08 CET 2012
Version: ""
PrintFootLine: no
actor: u, user, "", 50, 50
process: m, login_machine, "", 50, 50
process: udb, user_database
msg: u, m, login;
fragmentbegin: alt, u, udb, alt;
fragmenttext: alt, !user_exists;
msg: m, udb, get_login_data
regionbegin: udb, activation;
regionend:udb
msg: udb, m, user_not_exist,*;
msg: m, u, user_not_exist_or_data_fault; 
fragmentseparator:alt;
fragmenttext: alt, user_exists;
msg: m, udb, get_login_data
regionbegin: udb, activation;
regionend:udb
msg: udb,m,login_data,*;
msg: m, u, login_confirmed;
fragmentend: alt
