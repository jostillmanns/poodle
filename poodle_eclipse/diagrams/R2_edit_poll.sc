DiagramStyle: uml
DiagramName: R2_edit_poll
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:41:11 CET 2012
Version: ""
PrintFootLine: no
actor: u, poll_creator,"", 50, 50
process: m, edit_poll_machine,"", 50, 50
process: p, poll,"", 50, 50
msg: u, m, edit_poll;
fragmentbegin: alt, u, p, alt;
fragmenttext: alt, !poll_exists;
msg: m, p, edit_poll
regionbegin: p, activation;
regionend:p
msg:p,m,poll_not_found,*;
msg:m,u, poll_not_found;
fragmentseparator: alt;
fragmenttext: alt, poll_exists;
msg: m, p, edit_poll
regionbegin: p, activation;
regionend:p
msg:p,m,poll_changed,*;
msg:m,u, poll_changed;
fragmentend: alt
