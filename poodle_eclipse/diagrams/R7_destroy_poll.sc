DiagramStyle: uml
DiagramName: R7_destroy_poll
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:42:40 CET 2012
Version: ""
PrintFootLine: no
actor: u, poll_creator,"", 50, 50
process: m, destroy_machine,"", 50, 50
process: p, poll,"", 50, 50
msg: u, m, destroy_poll;
fragmentbegin: alt, u, p, alt;
fragmenttext: alt, !poll_exists;
msg: m, p, destroy_poll
regionbegin: p, activation;
regionend:p
msg:p,m,poll_not_found,*;
msg:m,u, poll_not_found;
fragmentseparator: alt;
fragmenttext: alt, poll_exists;
msg: m, p, destroy_poll
regionbegin: p, activation;
regionend:p
msg:p,m,poll_destroyed,*;
msg:m,u, poll_destroyed;
fragmentend: alt
