DiagramStyle: uml
DiagramName: R3_vote
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:41:11 CET 2012
Version: ""
PrintFootLine: no
actor: u, user, "", 50, 50
process: m, vote_machine, "", 50, 50
process: v, vote, "", 50, 50
process: p, poll
msg: u, m, vote;
msg: m, v, create vote
regionbegin: v, activation;
;
;
regionend: v
msg: v, m,"", *;
msg: m, p, place_vote
regionbegin: p, activation;
;
;
regionend: p
msg: p,m, "", *;
msg:m, u, voting_done;
