DiagramStyle: uml
DiagramName: R1_create_poll
PageSize: auto, auto
PageMargins: 10, 10, 10, 10
Font: "Arial", "16", "Regular"
LineOffset: 20
Author:
Company:
Date: Tue Feb 21 19:23:06 CET 2012
Version: ""
PrintFootLine: no
actor: u, user, ""
process: m, create_poll_machine
process: p, poll
state: m, !poll_exists;
msg: u, m, create_poll;
msg: m, p, making_poll
regionbegin: p, activation;
;
;
regionend: p
msg: p,m, "", *;
msg: m, u, confirm_create_poll;
state: m, poll_exists
